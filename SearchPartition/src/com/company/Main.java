package com.company;

import java.io.InputStreamReader;
import java.util.*;

public class Main {

    private static int K = 0; //Число разбиений

    private static Integer sum(ArrayList<Integer> array) //Метод поиска суммы элементов массива
    {
        int sum = 0;

        for (Integer element : array) {
            sum += element;
        }

        return sum;
    }

    static boolean isKPartitionPossible(ArrayList<Integer> array, int N) //Метод проверки на возможность разбиения
    {
        //Если k = 1, то наш массив - наш ответ
        if (K == 1) {
            return true;
        }
        //Если число множеств разбиения больше, чем число элементов множества, то разбиение невозомжно
        if (N < K) {
            return false;
        }

        // Если сумма элементов множества не делится нацело на К, то разбиение на К подмножеств не возможно

        int sum = sum(array);
        if (sum % K != 0) {
            return false;
        }

        // Сумма каждого подмножества должна быть равна sum/K
        int subset = sum / K;
        int[] subsetSum = new int[K];
        boolean[] taken = new boolean[N];

        //Инициализируем суммы всех подмножеств нулями
        for (int i = 0; i < K; i++) {
            subsetSum[i] = 0;
        }

        //Заполняем массив меток
        for (int i = 0; i < N; i++) {
            taken[i] = false;
        }

        subsetSum[0] = array.get(N - 1);
        taken[N - 1] = true;

        return isKPartitionPossibleRec(array, subsetSum, taken, subset, N, 0, N - 1);
    }

    static boolean isKPartitionPossibleRec(ArrayList<Integer> array, int subsetSum[], boolean taken[], int subset, int N, int curIdx, int limitIdx) {
        /*Общая логика такова: если число подмножеств, чья сумма равна subset, равно K-1, значит
         * мы можем разделить наше множество на K частей с равной суммой*/
        if (subsetSum[curIdx] == subset) {
            //Тут идет проверка на K - 2, потому что сумма последнего подмножества уже равна subset
            if (curIdx == K - 2) {
                return true;
            }

            // Иначе - заполняем подмножества дальше
            return isKPartitionPossibleRec(array, subsetSum, taken, subset, N, curIdx + 1, N - 1);
        }

        // Заполняем текущее подмножество элементами, которые еще не использовались
        for (int i = limitIdx; i >= 0; i--) {
            if (taken[i])
                continue;
            int tmp = subsetSum[curIdx] + array.get(i);

            if (tmp <= subset) {
                taken[i] = true;
                subsetSum[curIdx] += array.get(i);
                boolean nxt = isKPartitionPossibleRec(array, subsetSum, taken, subset, N, curIdx, i - 1);
                taken[i] = false;
                subsetSum[curIdx] -= array.get(i);
                if (nxt) {
                    return true;
                }
            }
        }
        return false;
    }

    private static void print(ArrayList<ArrayList<Integer>> subsets) {
        System.out.println("Разбиение на " + K + " подмножеств с одинаковой суммой " + " возможно:");
        System.out.print("{");
        for (ArrayList<Integer> arr : subsets) {
            System.out.print("{ ");
            for (Integer element : arr) {
                System.out.print(element + " ");
            }
            System.out.print("} ");
        }
        System.out.print("}");
    }

    //Жадный алгоритм для разбиения множества на K подмножеств
    private static ArrayList<ArrayList<Integer>> GreedyAlgorithm(ArrayList<Integer> array) {
        ArrayList<ArrayList<Integer>> resultArray = new ArrayList<>(K); //Список подмножеств

        //Добавляем первые K элементов по невозрастанию в подмножества
        for (int i = 0; i < K; i++) {
            resultArray.add(new ArrayList<>());
            resultArray.get(i).add(array.get(i));
        }

        //Добавляем по элементы по порядку в подмножество с наименьшей суммой
        for (int i = K; i < array.size(); i++) {
            Collections.sort(resultArray, new Comparator<ArrayList<Integer>>() {
                @Override
                public int compare(ArrayList<Integer> o1, ArrayList<Integer> o2) {
                    return Integer.compare(sum(o1), sum(o2));
                }
            });

            resultArray.get(0).add(array.get(i));
        }

        return resultArray;
    }

    public static void main(String[] args) {

        ArrayList<Integer> array = new ArrayList<>();

        Scanner scanner = new Scanner(new InputStreamReader(System.in));
        System.out.println("Введите размер множества:");
        int n = scanner.nextInt();
        System.out.println("Введите число подмножеств:");
        K = scanner.nextInt();

        System.out.println("Введите элементы множества:");
        for (int i = 0; i < n; i++) {
            array.add(scanner.nextInt());
        }

        System.out.println("Исходное множество: ");
        for (Integer element : array) {
            System.out.print(element + " ");
        }
        System.out.println();

        Collections.sort(array, Collections.reverseOrder());

        int sum = sum(array);

        if (isKPartitionPossible(array, array.size())) {
            print(GreedyAlgorithm(array));
        } else {
            System.out.println("Невозможно разделить данное множетсво на " + K + " равных частей");
        }
    }
}
